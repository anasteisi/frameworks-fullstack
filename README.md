# terminal
```
home 
cd 5g
cd frameworks-fullstack
flask run --host=0.0.0.0 --port=5030 >> flask.log 2>&1 & echo $! > flask.pid
cat flask.pid
ps -ef | grep 15625

./telegraf/usr/bin/telegraf --config telegraf.conf

GoTo http://85.192.35.28:5030/api/cars
GoTo http://85.192.35.28:3000/d/l2QxccaWk/red30?orgId=1&refresh=5s

kill 15625
```

# Running application
Our application now has frontend talking to our backend api. Navigate to http://SERVER_IP:YOUR_PORT/
## Install requirements  
```
# Install requirements
pip3 install -r requirements.txt --user
# Create ablemic table in db (on first run only)
flask db init
# Create migration file (if we changed model)
flask db migrate
# Apply migration to database
flask db upgrade
# Run tests
python3 tests.py
# Run app
flask run -- host=0.0.0.0 port=YOUR_PORT
```

# Frontend
```
# cd to frontend source folder
cd frameworks-fullstack/frontend

# Install dependances
npm install

# Run dev server, port will be written to console. Dev server supports hot reload
npm run dev

# Build bundle
npm run build
# Copy to flask
cp -R dist/* ../static/
```
